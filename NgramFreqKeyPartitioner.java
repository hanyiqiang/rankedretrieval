import org.apache.hadoop.mapreduce.Partitioner;

public class NgramFreqKeyPartitioner extends Partitioner<NgramFreqKey, NgramFreq>{

	@Override
	public int getPartition(NgramFreqKey key, NgramFreq value, int numPartitions) {
		return (key.getNgram().hashCode() & Integer.MAX_VALUE) % numPartitions;
	}
  
}