import org.apache.hadoop.mapreduce.*;

import java.io.Serializable;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

public class NgramFreqKeyGroupComparator extends WritableComparator implements Serializable {
  
	NgramFreqKeyGroupComparator() {
		super(NgramFreqKey.class, true);
	}

	@Override
	public int compare(WritableComparable a, WritableComparable b) {
		NgramFreqKey gka = (NgramFreqKey) a;
		NgramFreqKey gkb = (NgramFreqKey) b;

		return gka.getNgram().compareTo(gkb.getNgram());
	}
}