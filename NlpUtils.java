import java.util.Arrays;
import java.util.List;

public class NlpUtils {
	private static final String[] STOPWORDS = {
			"a", "an", "and", "are", "as", "at", 
			"be", "by", "for", "from", "has", "he", "her",
			"him", "his", "i", "in", "is", "it", 
			"its", "of", "on", "that", "the", "to", 
			"was", "were", "will", "with", "you", "your",
			".", ",", "!", "?", ":", ";", "'", "\"", "-"
	};
	
	private static final List<String> STOPWORD_LIST = Arrays.asList(NlpUtils.STOPWORDS);
	
	// public static List<String> replaceSingleCharacters(List<String> tokens) {
		// tokens.replaceAll("^[a-zA-Z]$", "!").replaceFirst();
		// return tokens;
	// }
	
	public static List<String> removeStopwords(List<String> tokens) {
		tokens.removeAll(NlpUtils.STOPWORD_LIST);
		return tokens;
	}
	
	// public static List<String> toLower(List<String> tokens) { 
		// tokens.replaceAll( "[^A-Za-z \n]", "" ).toLowerCase(); 
		// return tokens;
	// }
	// public static List<String> cleanTokens(List<String> tokens) { 
		// tokens.removeAll("");
		// return tokens;
	// }
}
