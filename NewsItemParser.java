

import java.io.ByteArrayInputStream;
import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;



public class NewsItemParser {
	private SAXParserFactory factory;
	private SAXParser parser;
	private ByteArrayInputStream xmlStream;
	private NewsItemHandler handler;
	public int tokenPos;
	
	public NewsItemParser() {
		try {
			factory = SAXParserFactory.newInstance();
			parser = factory.newSAXParser();
			handler = new NewsItemHandler();
			// tokenPos = factory.getLocation().getCharacterOffset();	// the compiler complained "symbol:   method getLocation()  location: variable factory of type SAXParserFactory", so I commented this line temporarily
		} catch (ParserConfigurationException | SAXException e) {
			e.printStackTrace();
		}
	}
	
	public NewsItem parseArticle(byte[] xml) {
		try {
			xmlStream = new ByteArrayInputStream(xml);
			parser.parse(xmlStream, handler);
			xmlStream.reset();
			xmlStream.close();
			return handler.getArticle();
		} catch (SAXException | IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	// Seems this returns the xml end line location within the doc...
	// public int NewsItem getPos(byte[] xml) {
		// try {
			// xmlStream = new ByteArrayInputStream(xml);
			// XMLStreamReader xmlStreamReader = XMLInputFactory.newInstance().createXMLStreamReader(xmlStream);
			// return xlMStreamReader.getLocation();
		// } catch (SAXException | IOException e) {
			// e.printStackTrace();
			// return null;
		// }
	// }
}
