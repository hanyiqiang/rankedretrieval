import java.io.IOException;
import java.io.DataInput;
import java.io.DataOutput;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

public class NgramFreqKey implements WritableComparable<NgramFreqKey> {

	private Text ngram;
	private Text clusterId;

	public NgramFreqKey(){
		set(new Text(), new Text());
	}	// new empty record
  
	public NgramFreqKey (Text ngram, Text clusterId) {
		set(ngram, clusterId);
	}	// existing record

	public NgramFreqKey (String ngram, String clusterId) {
		set(new Text(ngram), new Text(clusterId));
	}	// new string record
  
	public void set(Text ngram, Text clusterid){
		this.ngram = ngram;
		this.clusterId = clusterid;
	}	// set new value

	@Override
	public void readFields(DataInput in) throws IOException {
		ngram.readFields(in);
		clusterId.readFields(in);
	}

	@Override
	public void write(DataOutput out) throws IOException {
		ngram.write(out);
		clusterId.write(out);
	}

	public Text getNgram() {
		return ngram;
	}

	public Text getClusterId() {
		return clusterId;
	}
  
	@Override
	public int compareTo(NgramFreqKey o) {
		if (ngram.compareTo(o.getNgram()) != 0) {
			return ngram.compareTo(o.getNgram());	// first compare term
		} else {
			return clusterId.compareTo(o.getClusterId());	// if same term, then compare docID
		}
	}
}