// import java.io.IOException;
// import java.io.InputStream;
// import java.io.OutputStream;
import java.io.*;
import java.util.*;
import javax.xml.stream.*;
import static javax.xml.stream.XMLStreamConstants.*;

import org.apache.hadoop.conf.*;
import org.apache.hadoop.fs.*;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.*;
import org.apache.hadoop.mapreduce.lib.output.*;
import org.apache.hadoop.util.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.mapred.FileSplit;

/* MAP */
/**
 * @SuppressWarnings( "deprecation" )
 */
public class TfIdfMapper 
		extends Mapper<Text, BytesWritable, NgramFreqKey,NgramFreq> {			
	// 3 fields in NgramFreq
	private Text ngram;
	private Text docID;	// also named clusterId
	private IntWritable count;
	
	private NewsItemParser parser;
	private NewsItem article;
	
	private int numEntries = 0;	// number of file entries
	
	public NgramFreqKey ngramFreqKey;
	public NgramFreq ngramFreq;
	
	@Override
	protected void setup(Context context) 
			throws IOException, InterruptedException {
		ngram = new Text();
		docID = new Text();
		// count = new IntWritable[1];
		count = new IntWritable(1);
		
		parser = new NewsItemParser();
		// tokenizer = SimpleTokenizer.INSTANCE;
		ngramFreqKey = new NgramFreqKey();
		ngramFreq = new NgramFreq();
	}
	
	public void map(Text key, BytesWritable value, Context context) 
			throws IOException, InterruptedException {
		// NOTE: the filename is the *full* path within the ZIP file
		// e.g. "subdir1/subsubdir2/Ulysses-18.txt"
		String filename = key.toString();
		// LOG.info( "   ... Mapping files" );
		// LOG.info( "  ... map: " + filename );
		
		// Only want to process .txt files
		// if ( filename.endsWith(".txt") == false )
			// return;
		// Only want to process .xml files
		if ( filename.endsWith(".xml") == false )
			return;
		else
			numEntries++;
		
		// Prepare the content 
		// Original
		// String content = new String( value.getBytes(), "UTF-8" );
			
		long startTime = System.nanoTime();
		
		// Get article content
		article = parser.parseArticle(value.getBytes());
		docID.set(Integer.toString(article.getID()));
		String content = article.getTitle() + " " + article.getText();
		content = content.replaceAll( "[^A-Za-z \n]", "" ).toLowerCase();
		
		// ArrayList<String> wordList = new ArrayList<String>(Arrays.asList(content.split("\\s+")));	//split the string by a whitespace      
		String wordList[] = content.split("\\s+");	//split the string by a whitespace 

		// NlpUtils.replaceSingleCharacters(wordList);
		// NlpUtils.removeStopwords(wordList);	// only applies to List<String>, but I got String[], not using it for now.
		
		//Time it
		long endTime = System.nanoTime();

		long duration = (endTime - startTime)/1000000; 
		System.out.print("Processed XML File " + numEntries + " in " + duration + "ms ");
		
		// write the parsed words into key/value pairs

		for(String word : wordList) {
			if (word != null && !word.isEmpty()) {
				ngram.set(word);
				
				ngramFreqKey.set(ngram,docID);
				ngramFreq.set(ngram,docID,count);

				context.write(ngramFreqKey, ngramFreq);	// store mapped data
			}
		// while (tokenizer.hasMoreTokens()) {
			// word.set(tokenizer.nextToken());	
			// Word Count Example
			// context.write(word, new IntWritable(1));
		}
		// context.write(ngramFreqKey, ngramFreq);	// store mapped data
	}
}