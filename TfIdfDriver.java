import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.*;
import java.util.HashMap;
import javax.xml.stream.*;
import static javax.xml.stream.XMLStreamConstants.*;

import org.apache.hadoop.conf.*;
import org.apache.hadoop.fs.*;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.*;
import org.apache.hadoop.mapreduce.lib.output.*;
import org.apache.hadoop.util.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.mapred.FileSplit;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.MapWritable;
import org.apache.hadoop.io.DoubleWritable;

public class TfIdfDriver extends Configured implements Tool {
	// prepare log file
	private static final Log LOG = LogFactory.getLog(TfIdfDriver.class);
	
	/* MAIN */
	public static void main(String[] args) throws Exception{
		int res = ToolRunner.run(new TfIdfDriver(), args);
		System.exit(res);
	}
  
	@Override
	public int run(String[] args) throws Exception {
		// if (args.length != 2) {
			// System.err.println("Usage: TfIdfDriver <input> <output>");
			// return -1;
		// }
    
		// Path input = new Path(args[0]);
		// Path output = new Path(args[1]);
		Path inputPath = new Path("/tmp/Data/");
		Path outputPath = new Path("/tmp/" + this.getClass().getName());
		
		// mapper job 1: count total doc numbers
		long docCount = countTotalDocuments(inputPath);	// only need mapper phase, deactivate reducers, job.setNumReduceTasks(0);
		String docCountStr = Long.toString(docCount);
		LOG.info( "   ... Total Document Number is: " + docCountStr );
		// mapper job 2: calculate tf-idf values
		return calculateTfIdf(inputPath, 7, docCount);	// here 7 is the reduceTasks input argument. controling reducer size? Can be omitted?
	}
	
	
	/* JOB1 COUNT TOTAL DOC NUMBER */
	// We put this as a single job because if put in the same mapper job, the reduce job may kick in before the mapper finishes, in that case we will not be able to get the total doc number//
	public long countTotalDocuments(Path inputPath) throws IOException, 
				InterruptedException, ClassNotFoundException {
    
		Configuration conf = new Configuration();
		
		LOG.info( "Preparing FileSystem for tests" );
		FileSystem fs = FileSystem.get(conf);
		
		Path outputPath = new Path("/tmp/" + this.getClass().getName());	// temporarily copy from main run
		// Delete our working directory if it already exists
		LOG.info( "   ... Deleting " + outputPath.toString() );
		fs.delete(outputPath, true);
		Job job = new Job(conf);
		
		//set job 1 name
		job.setJobName(TfIdfDriver.class.getSimpleName() + ".countTotalDocuments: " + inputPath);
		job.setJarByClass(TfIdfDriver.class);
		
		// for first job, no need to send it to reducer
		job.setMapOutputKeyClass(NullWritable.class);
		job.setMapOutputValueClass(NullWritable.class);
		//  output formats
		job.setOutputFormatClass(NullOutputFormat.class);
		// job.setOutputKeyClass(Text.class);
		// job.setOutputValueClass(NullWritable.class);
		
		//rather it has to go thru combiner to get the total doc numbers
		job.setMapperClass(DocumentCountMapper.class);
		// job.setCombinerClass(DocumentCountCombiner.class);
		// job.setReducerClass(DocumentCountReducer.class);
		job.setNumReduceTasks(0);
		
		//  input formats
		job.setInputFormatClass(ZipFileInputFormat.class);
		ZipFileInputFormat.setInputPaths(job, inputPath);
		// Reset ZipFileInputFormat leniency (true): tolerate unrecognized files, corrupt zip files etc.
        ZipFileInputFormat.setLenient( true );
		// Copy the test files
		LOG.info( "   ... Copying files" );
		fs.mkdirs(inputPath);
		copyFile(fs, inputPath, "19960824.zip", conf);
		copyFile(fs, inputPath, "19960825.zip", conf);
		fs.close();
		
		

		boolean succeeded = job.waitForCompletion(true);
		if (!succeeded) {
		  throw new IllegalStateException("Job failed!");
		}

		return job.getCounters().findCounter(DocumentCountMapper.Count.TOTAL_DOCUMENTS)
				.getValue();
	}
	
	
	
	/* JOB2 CALCULATE TF-IDF PER TERM IN SINGLE DOC */
	private int calculateTfIdf(Path inputPath, int reduceTasks, long docCount) 
		  throws IOException, InterruptedException, ClassNotFoundException {
    
		Configuration conf = new Configuration();
		// conf.setLong(TfIdfDriver.TOTAL_DOCUMENTS, docCount);
		conf.setLong("TOTAL_DOCUMENTS", docCount);
		
		Job job = new Job(conf);
		
		Path outputPath = new Path("/tmp/" + this.getClass().getName());	// temporarily copy from main run
		job.setJobName(TfIdfDriver.class.getSimpleName() + ".calcTfIdf: " + inputPath);
		job.setJarByClass(TfIdfDriver.class);

		//  input formats
		job.setInputFormatClass(ZipFileInputFormat.class);
		ZipFileInputFormat.setInputPaths(job, inputPath);
		// Reset ZipFileInputFormat leniency (true): tolerate unrecognized files, corrupt zip files etc.
        ZipFileInputFormat.setLenient( true );
		
		//  output formats
		job.setMapOutputKeyClass(NgramFreqKey.class);
		job.setMapOutputValueClass(NgramFreq.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(DoubleWritable.class);
		
		// this output format correspondes to the final output from the TfIDFReducer, for now, let's assume it is plain text output, later will change it to Hivedatabase output (Hcatelog)
		TextOutputFormat.setOutputPath(job, outputPath);

		job.setMapperClass(TfIdfMapper.class);		
		// job.setCombinerClass(TfIdfReducer.class);	// do not include this line if plan to use different output format than mapper output format
		job.setPartitionerClass(NgramFreqKeyPartitioner.class);
		job.setGroupingComparatorClass(NgramFreqKeyGroupComparator.class);
		job.setReducerClass(TfIdfReducer.class);
		
		return job.waitForCompletion(true) ? 0 : 1;
	}
	
	/**
     * Simple utility function to copy files into HDFS
     *
     * @param fs
     * @param name
	 * @param inputPath
	 * @param conf
     * @throws IOException
     */
    private void copyFile(FileSystem fs, Path inputPath, String name, Configuration conf)
            throws IOException
    {
        LOG.info( "copyFile: " + name );
        InputStream is = this.getClass().getResourceAsStream( "/" + name );
        OutputStream os = fs.create( new Path(inputPath, name), true );
        IOUtils.copyBytes( is, os, conf );
        os.close();
        is.close();
    }
}