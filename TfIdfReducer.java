import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.*;
import javax.xml.stream.*;
import java.util.HashMap;
import java.math.*;
import static javax.xml.stream.XMLStreamConstants.*;

import org.apache.hadoop.conf.*;
import org.apache.hadoop.fs.*;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.*;
import org.apache.hadoop.mapreduce.lib.output.*;
import org.apache.hadoop.util.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.mapred.FileSplit;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.MapWritable;
import org.apache.hadoop.io.DoubleWritable;

public class TfIdfReducer extends Reducer<NgramFreqKey, NgramFreq, Text, DoubleWritable> {
	
	public static final String TOTAL_DOCUMENTS = "TOTAL_DOCUMENTS";
	private long totalDocuments;
	private double tf;
	private double idf;
	private DoubleWritable tfidf;
	
	@Override
	protected void setup(Context context) throws IOException, InterruptedException {
		super.setup(context);
		Configuration conf = context.getConfiguration();
		this.totalDocuments = conf.getLong(TOTAL_DOCUMENTS, -1);
	}	// get the total doc number
	
	
	@Override
	public void reduce(NgramFreqKey key, Iterable<NgramFreq> values, Context context) throws 			IOException,InterruptedException {
		Text currentNgramText = key.getNgram();
		Text currentDocIdText = new Text();
		String currentNgram = currentNgramText.toString();
		String currentDocId = null;
		
		long documentCount = 0;
		long termCount = 0;
		long totalFreq = 0;
		// LongWritable termCountWritable = new LongWritable();
		// NgramFreqKey ngramFreqKey = new NgramFreqKey();
		// NgramFreq ngramFreq = new NgramFreq();
		// HashMap<String, Long> termFreqs = new HashMap<>();
		HashMap<Text, LongWritable> termFreqs = new HashMap<>();
		MapWritable outputMap = new MapWritable();
		while(values.iterator().hasNext()) {
			NgramFreq ngramFreq = values.iterator().next();
			long freq = ((Number)ngramFreq.getCount().get()).longValue();
			String docId = ngramFreq.getClusterId().toString();
						
			currentDocIdText = ngramFreq.getClusterId();
			
			
			if (!ngramFreq.getNgram().toString().equals(currentNgram)) {
				throw new IllegalStateException("current: " + currentNgram
						+ "tthis: " + ngramFreq.getNgram().toString());
			}

			if (currentDocId == null || !currentDocId.equals(docId)) {
				if(currentDocId != null){
					// termFreqs.put(currentDocId, termCount);
					LongWritable termCountWritable = new LongWritable(termCount);
					termFreqs.put(currentDocIdText, termCountWritable);
					// termFreqs.put(termCountWritable, termCountWritable);
				}
				currentDocId = docId;
				documentCount += 1;
				termCount = freq;
				totalFreq += freq;
				
				} else {
					termCount += freq;
					totalFreq += freq;
			}
			
			// LongWritable termCountWritable = new LongWritable(termCount);
			
			// outputMap.putAll(termFreqs);
			// context.write(currentNgramText, outputMap);
			tf = 1+Math.log10(termCount);
			// double totDoc = (double) totalDocuments;
			// double docCount = (double) documentCount;
			// idf = Math.log10(totDoc/docCount);
			idf = Math.log10(totalDocuments/documentCount);
			tfidf = new DoubleWritable(tf*idf);
			context.write(currentNgramText, tfidf);
		}
		// context.write(currentNgramText, currentDocIdText);
		
	}
}