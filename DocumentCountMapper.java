import java.io.IOException;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class DocumentCountMapper extends Mapper<Text, BytesWritable, Text, NullWritable> {
	public enum Count {
		TOTAL_DOCUMENTS
	}
	// @Override
	public void map(Text key, BytesWritable value, Context context) throws IOException, InterruptedException {
  		context.getCounter(Count.TOTAL_DOCUMENTS).increment(1);
	}
}