#!/bin/bash
#source setenv.sh
# javac *.java

# Class directory
# export HADOOP_CLASSPATH=${HADOOP_CLASSPATH}:/home/${USER}/TextMining/*

cd /home/yiqianh/TextMining/rankedRetrieval

hadoop com.sun.tools.javac.Main TfIdfDriver.java
read -p "  ...Compile classes DONE, Press Enter to continue, Ctrl+C to abort"
jar cf TfIdfDriver.jar *.class
read -p "  ...Generate jar file DONE, Press Enter to continue, Ctrl+C to abort"
hadoop jar TfIdfDriver.jar TfIdfDriver
read -p "  ...Run job DONE, Press Enter to continue, Ctrl+C to abort"
hadoop fs -ls /tmp/TfIdfDriver
read -p "  ...Show results, Press Enter to continue, Ctrl+C to abort"
hadoop fs -tail /tmp/TfIdfDriver/part-r-00000