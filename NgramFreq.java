import java.io.IOException;
import java.io.DataInput;
import java.io.DataOutput;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

public class NgramFreq implements Writable{
  
	private Text ngram;
	private Text clusterId;
	private IntWritable count;

	public NgramFreq(){
		set(new Text(), new Text(), new IntWritable());
	}

	public NgramFreq(Text ngram, Text clusterId, IntWritable count) {
		set(ngram, clusterId, count);
	}

	public NgramFreq(String ngram, String clusterId, int count) {
		set(new Text(ngram), new Text(clusterId), new IntWritable(count));
	}
  
	public void set(Text ngram, Text clusterid, IntWritable count){
		this.ngram = ngram;
		this.clusterId = clusterid;
		this.count = count;
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		ngram.readFields(in);
		clusterId.readFields(in);
		count.readFields(in);
	}

	@Override
	public void write(DataOutput out) throws IOException {
		ngram.write(out);
		clusterId.write(out);
		count.write(out);
	}

	public Text getNgram() {
		return ngram;
	}

	public Text getClusterId() {
		return clusterId;
	}

	public IntWritable getCount(){
		return count;
	}
}