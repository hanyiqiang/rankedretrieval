import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


// This class parses an XML document from RCV2 into a NewsItem which captures 
// the context text, title, and item id
public class NewsItemHandler extends DefaultHandler {
	static final String newsTag = "newsitem";
	static final String titleTag = "headline";
	static final String lineTag = "p";
	
	private NewsItem article;
	
	// Java String concat is O(n^2) but StringBuffer gives us thread-safe O(n)
	private StringBuffer text;
	private String title;
	private int id;
	
	private boolean foundText = false;
	private boolean foundTitle = false;
	
	public NewsItemHandler() {
		article = new NewsItem();
		text = new StringBuffer();
	}
	
	public NewsItem getArticle() {
		return article;
	}
	
	@Override
	// Note we need to set flags for each line of content text we see <p> to 
	// add them into our StringBuilder. Also need to flag down the title
	public void startElement(String uri, String localName, String qName, Attributes attributes) 
			throws SAXException {
		// Check for tags we're interested in
		if(qName.equalsIgnoreCase(NewsItemHandler.newsTag)) {
			id = Integer.parseInt(attributes.getValue("itemid"));
			article.setID(id);
		} else if (qName.equalsIgnoreCase(NewsItemHandler.titleTag)) {
			foundTitle = true;
		} else if (qName.equalsIgnoreCase(NewsItemHandler.lineTag)) {
			foundText = true;
		}
	}
	
	@Override
	public void endElement(String uri, String localName, String qName) 
			throws SAXException {
		if(qName.equalsIgnoreCase(NewsItemHandler.newsTag)) {
			article.setText(text.toString());
			text.setLength(0);
		} else if (qName.equalsIgnoreCase(NewsItemHandler.titleTag)) {
			article.setTitle(title);
		}
	}
	
	public void characters(char[] ch, int start, int length)
			throws SAXException	{
		// Grab text out of tag
		if(foundTitle) {
			title = new String(ch, start, length);
			foundTitle = false;
		} else if (foundText) {
			text.append(ch, start, length);
			text.append(" ");				// Each paragraph separated by space
			foundText = false;
		}
	}
}
